/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aprendendojava.positiveintegers;

import java.util.Stack;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author luiz
 */
public class PositiveIntegerMethodsTest {
    
    @Test
    public void gdcTest() {
        Assert.assertEquals("gdc(2,6)", 2, PositiveIntegerMethods.gdc(2,6));
        Assert.assertEquals("gdc(5,17)", 1, PositiveIntegerMethods.gdc(5,17));
        Assert.assertEquals("gdc(100,10)", 10, PositiveIntegerMethods.gdc(100,10));
        Assert.assertEquals("gdc(0,4)", 4, PositiveIntegerMethods.gdc(0,4));
        Assert.assertEquals("gdc(0,0)", 0, PositiveIntegerMethods.gdc(0,0));
    }
    
    @Test
    public void gdcEqSolutionTest() {
        Integer[] array = {4, -3, 1};
        Assert.assertArrayEquals("gdc(13,17)", array, PositiveIntegerMethods.gdcEqSolution(13, 17));
//        array = {1, 0, 13};
        array[0] = 1;
        array[1] = 0;
        array[2] = 13;
        Assert.assertArrayEquals("gdc(13,52)", array, PositiveIntegerMethods.gdcEqSolution(13, 52));
    }
    
    @Test
    public void gdcEqSolutionReductor () {
        Stack<Integer> ys = new Stack<>();
        Stack<Integer> zs = new Stack<>();
        PositiveIntegerMethods.gdcEqSolutionReductor(ys, zs, 13, 17);
        zs.pop();
        Assert.assertEquals("reductor(17, 13)", (Integer) PositiveIntegerMethods.gdc(17, 13), zs.peek());
    }

    @Test
    public void roundupTest() {
        Assert.assertEquals("roundup(15,6)", 18, PositiveIntegerMethods.roundup(15,6));
        Assert.assertEquals("roundup(4,4)", 4, PositiveIntegerMethods.roundup(4,4));
        Assert.assertEquals("roundup(0,10)", 0, PositiveIntegerMethods.roundup(0,10));
    }

    @Test
    public void rounddownTest() {
        Assert.assertEquals("rounddown(15,6)", 12, PositiveIntegerMethods.rounddown(15,6));
        Assert.assertEquals("rounddown(4,4)", 4, PositiveIntegerMethods.rounddown(4,4));
        Assert.assertEquals("rounddown(0,10)", 0, PositiveIntegerMethods.rounddown(0,10));
    }
    
    @Test
    public void roundTest() {
        Assert.assertEquals("round(15,6)", 18, PositiveIntegerMethods.round(15,6));
        Assert.assertEquals("round(16,6)", 18, PositiveIntegerMethods.round(16,6));
        Assert.assertEquals("round(14,6)", 12, PositiveIntegerMethods.round(12,6));
        Assert.assertEquals("round(4,4)", 4, PositiveIntegerMethods.round(4,4));
        Assert.assertEquals("round(0,10)", 0, PositiveIntegerMethods.round(0,10));
    }
    
    @Test
    public void factorialTest() {
        Assert.assertEquals("0! = 1", 1, PositiveIntegerMethods.factorial(0));
        Assert.assertEquals("1! = 1", 1, PositiveIntegerMethods.factorial(1));
        Assert.assertEquals("2! = 2", 2, PositiveIntegerMethods.factorial(2));
        Assert.assertEquals("3! = 6", 6, PositiveIntegerMethods.factorial(3));
        Assert.assertEquals("4! = 24", 24, PositiveIntegerMethods.factorial(4));
        Assert.assertEquals("5! = 120", 120, PositiveIntegerMethods.factorial(5));
        Assert.assertEquals("6! = 720", 720, PositiveIntegerMethods.factorial(6));
        Assert.assertEquals("7! = 5040", 5040, PositiveIntegerMethods.factorial(7));
        Assert.assertEquals("8! = 40320", 40320, PositiveIntegerMethods.factorial(8));
    }
    
    @Test
    public void fibonacciTest() {
        Assert.assertEquals("F0 = 0", 0, PositiveIntegerMethods.fibonacciRecursive(0));
        Assert.assertEquals("F1 = 1", 1, PositiveIntegerMethods.fibonacciRecursive(1));
        Assert.assertEquals("F2 = 1", 1, PositiveIntegerMethods.fibonacciRecursive(2));
        Assert.assertEquals("F3 = 2", 2, PositiveIntegerMethods.fibonacciRecursive(3));
        Assert.assertEquals("F4 = 3", 3, PositiveIntegerMethods.fibonacciRecursive(4));
        Assert.assertEquals("F5 = 5", 5, PositiveIntegerMethods.fibonacciRecursive(5));
        Assert.assertEquals("F6 = 8", 8, PositiveIntegerMethods.fibonacciRecursive(6));
        Assert.assertEquals("F7 = 13", 13, PositiveIntegerMethods.fibonacciRecursive(7));
        Assert.assertEquals("F8 = 21", 21, PositiveIntegerMethods.fibonacciRecursive(8));
        Assert.assertEquals("F0 = 0", 0, PositiveIntegerMethods.fibonacciExplicit(0));
        Assert.assertEquals("F1 = 1", 1, PositiveIntegerMethods.fibonacciExplicit(1));
        Assert.assertEquals("F2 = 1", 1, PositiveIntegerMethods.fibonacciExplicit(2));
        Assert.assertEquals("F3 = 2", 2, PositiveIntegerMethods.fibonacciExplicit(3));
        Assert.assertEquals("F4 = 3", 3, PositiveIntegerMethods.fibonacciExplicit(4));
        Assert.assertEquals("F5 = 5", 5, PositiveIntegerMethods.fibonacciExplicit(5));
        Assert.assertEquals("F6 = 8", 8, PositiveIntegerMethods.fibonacciExplicit(6));
        Assert.assertEquals("F7 = 13", 13, PositiveIntegerMethods.fibonacciExplicit(7));
        Assert.assertEquals("F8 = 21", 21, PositiveIntegerMethods.fibonacciExplicit(8));
    }
    
    @Test
    public void digitSum() {
        Assert.assertEquals("Sum of digits of 1293", 15, PositiveIntegerMethods.digitSum(1293));
        Assert.assertEquals("Sum of digits of 829", 19, PositiveIntegerMethods.digitSum(829));
        Assert.assertEquals("Sum of digits of 1000000000", 1, PositiveIntegerMethods.digitSum(1000000000));
    }
    
    @Test
    public void isIncreasingTest() {
        int[] array1 = {1, 2, 3};
        Assert.assertTrue("[1, 2, 3] is increasing", PositiveIntegerMethods.isIncreasing(array1));
        int[] array2 = {1, 7, 3};
        Assert.assertFalse("[1, 7, 3] is not increasing", PositiveIntegerMethods.isIncreasing(array2));
    }
    
    @Test
    public void isDecreasingTest() {
        int[] array1 = {3, 2, 1};
        Assert.assertTrue("[3, 2, 1] is decreasing", PositiveIntegerMethods.isDecreasing(array1));
        int[] array2 = {1, 7, 3};
        Assert.assertFalse("[1, 7, 3] is not decreasing", PositiveIntegerMethods.isDecreasing(array2));
    }
    
    @Test
    public void powTest() {
        Assert.assertEquals(9, PositiveIntegerMethods.pow(3,2));
        Assert.assertEquals(1024, PositiveIntegerMethods.pow(2,10));
        Assert.assertEquals(125, PositiveIntegerMethods.pow(5,3));
    }
    
    @Test
    public void nDigits() {
        Assert.assertEquals(3, PositiveIntegerMethods.nDigits(392));
        Assert.assertEquals(2, PositiveIntegerMethods.nDigits(20));
        Assert.assertEquals(0, PositiveIntegerMethods.nDigits(00));
    }
}