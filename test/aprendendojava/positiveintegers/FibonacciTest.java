/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aprendendojava.positiveintegers;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author luiz
 */
public class FibonacciTest {
    
    @Test
    public void fibonacciExplicitTest() {
        Assert.assertEquals(3, PositiveIntegerMethods.fibonacciExplicit(4));
        Assert.assertEquals(PositiveIntegerMethods.fibonacciRecursive(10), PositiveIntegerMethods.fibonacciExplicit(10));
    }
    
    @Test
    public void fibonacciTest() {
        long timeRecursive = 0;
        long timeExplicit = 0;
        for (int n = 1; n < 30; n++) {

            timeRecursive = System.nanoTime();
            for (int i = 0; i < n; i++) {
                System.out.print("F("+i+")="+PositiveIntegerMethods.fibonacciRecursive(i));
                if (i < n-1) {
                    System.out.print(", ");
                } else {
                    System.out.print(".\n");
                }
            }
            timeRecursive = System.nanoTime() - timeRecursive;
            timeRecursive /= 1000;
            System.out.println("Time Recursive = " + timeRecursive);

            timeExplicit = System.nanoTime();
            for (int i = 0; i < n; i++) {
                System.out.print("F("+i+")="+PositiveIntegerMethods.fibonacciExplicit(i));
                if (i < n-1) {
                    System.out.print(", ");
                } else {
                    System.out.print(".\n");
                }
            }
            timeExplicit = System.nanoTime() - timeExplicit;
            timeExplicit /= 1000;
            System.out.println("Time Explicit = " + timeExplicit);

            if (timeRecursive <= timeExplicit) {
                System.out.println("Recursive method was faster for n = " + n);
            } else {
                System.out.println("Explicit method was faster for n = " + n);
            }
            
            System.out.println("******************************************");
            
        }
    }
    
}