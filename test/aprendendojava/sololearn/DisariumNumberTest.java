/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aprendendojava.sololearn;

import java.util.List;
import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author luiz
 */
public class DisariumNumberTest {
    
    @Test
    public void checkTest() {
        Assert.assertTrue(DisariumNumbers.check(135));
        Assert.assertTrue(DisariumNumbers.check(89));
        Assert.assertTrue(DisariumNumbers.check(1));
    }
    
    @Test
    public void listTest() {
        List<Integer> list = DisariumNumbers.list(1000000);
        list.forEach(i -> System.out.println(i));        
    }
    
}
