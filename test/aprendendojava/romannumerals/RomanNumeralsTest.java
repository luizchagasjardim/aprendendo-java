/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aprendendojava.romannumerals;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author luiz
 */
public class RomanNumeralsTest {
    
    @Test
    public void oneToFiveThousand () {
        for (int i = 1; i < 5000; i++) {
            Assert.assertTrue("Error converting " + i + "to Roman", RomanNumerals.isRomanNumeral(RomanNumerals.toRomanNumeral(i)));
        }
    }
    
    @Test
    public void NotRoman () {
        String strArray[] = {"1", "A", "MDDC", "IM", "VLX"};
        for (String str : strArray) {
            Assert.assertFalse(str + " was mistankenly flagged as Roman", RomanNumerals.isRomanNumeral(str));
        }
        
    }
}
