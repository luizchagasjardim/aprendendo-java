/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aprendendojava.codingbat.logic;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author luiz
 */
public class LuckySumTest {

    @Test
    public void Tests () {
        int array1[] = {0, 13, 1, 3, 5};
        Assert.assertEquals("Wrong sum for array1", 0, LuckySum.luckySum(array1));
        int array2[] = {1, 2, 3, 4};
        Assert.assertEquals("Wrong sum for array2", 10, LuckySum.luckySum(array2));
        int array3[] = {1, 2, 3, 4, 5, 13};
        Assert.assertEquals("Wrong sum for array3", 15, LuckySum.luckySum(array3));
    }

}