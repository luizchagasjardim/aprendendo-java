/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aprendendojava.codingbat.logic;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author luiz
 */
public class BrickRowTest {
    
    @Test
    public void makeBrickRowTest() {
        BrickRow br = new BrickRow(1,5);
        Assert.assertTrue("BrickRow 1*1 + 0*5 = 1", br.makeBrickRow(3, 0, 1));
        br = new BrickRow(3,4);
        Assert.assertTrue("BrickRow 4*3 + 7*4 = 40", br.makeBrickRow(6, 31, 40));
        
        //Falha no primeiro teste
        br = new BrickRow(1,5);
        Assert.assertFalse("BrickRow 1*1 + 1*5 < 10", br.makeBrickRow(1, 1, 10));
        
        //Falha no segundo teste
        Assert.assertFalse("Negative number of bricks", br.makeBrickRow(-1, 2, 3));
        
        //Falha no terceiro teste
        br = new BrickRow(2, 8);
        Assert.assertFalse("Incompatible gdc", br.makeBrickRow(100, 100, 37));
        
        //Falha no final
        br = new BrickRow(2,7);
        Assert.assertFalse("BrickRow a*2 + b*7 != 10", br.makeBrickRow(4, 100, 10));
    }
    
    
}
