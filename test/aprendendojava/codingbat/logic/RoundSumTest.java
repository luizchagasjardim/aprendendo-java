/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aprendendojava.codingbat.logic;

import org.junit.Test;
import org.junit.Assert;

/**
 *
 * @author luiz
 */
public class RoundSumTest {
    
    @Test
    public void roundSumTest() {
        int array1[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Assert.assertEquals("Wrong round sum for array1", 60 , RoundSum.roundSum(array1));
        int array2[] = {53, 47};
        Assert.assertEquals("Wrong round sum for array2", 100 , RoundSum.roundSum(array2));
        int array3[] = {1723, 2072, 1962};
        Assert.assertEquals("Wrong round sum for array3", 5750 , RoundSum.roundSum(array3));
    }

}
