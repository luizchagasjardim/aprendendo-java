/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aprendendojava.romannumerals;

import java.util.regex.Pattern;

/**
 *
 * @author luiz
 */
public class RomanNumerals {

    private static final Pattern ROMAN = Pattern.compile(
                "^(?=.)M*"
                + "(C[MD]|D?C{0,3})"
                + "(X[CL]|L?X{0,3})"
                + "(I[XV]|V?I{0,3})$");
    
    static boolean isRomanNumeral (String str) {
        return ROMAN.matcher(str).matches();
    }

    static String toRomanNumeral(int i) {
        String rmn = "";
        
        //milhares
        while (i >= 1000) {
            rmn += "M";
            i -= 1000;
        }
        
        //centenas
        rmn += digitToRoman(i/100, 'C', 'D', 'M');
        i = i - 100*(i/100);
        //dezenas
        rmn += digitToRoman(i/10, 'X', 'L', 'C');
        i = i - 10*(i/10);
        //unidades
        rmn += digitToRoman(i, 'I', 'V', 'X');
        
        return rmn;
    }
    
    private static String digitToRoman(int i, char A, char B, char C) {
        
        String rmn = "";
        
        if (i < 0 || i > 9) {
            System.err.println("erro");
            System.exit(1);
        }
        
        if (i > 4 && i < 9) {
            rmn += B;
        }
        
        switch (i) {
            case 4:
                rmn += A;
                rmn += B;
                break;
            case 9:
                rmn += A;
                rmn += C;
                break;
            case 8:
            case 3:
                rmn += A;
            case 7:
            case 2:
                rmn += A;
            case 6:
            case 1:
                rmn += A;
                break;
                
        }
        
        return rmn;
        
    }
}
