/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aprendendojava.phonenumber;

import java.util.Comparator;
import static java.util.Comparator.comparingInt;

/**
 *
 * @author luiz
 */
class PhoneNumber {

    int areaCode;
    int prefix;
    int lineNum;
    
    PhoneNumber(int _areaCode, int _prefix, int _lineNum) {
        areaCode = _areaCode;
        prefix = _prefix;
        lineNum = _lineNum;
    }

    private static final Comparator<PhoneNumber> COMPARATOR
            = comparingInt((PhoneNumber pn) -> pn.areaCode)
                    .thenComparingInt(pn -> pn.prefix)
                    .thenComparingInt(pn -> pn.lineNum);

    public int compareTo(PhoneNumber pn) {
        return COMPARATOR.compare(this, pn);
    }

}
