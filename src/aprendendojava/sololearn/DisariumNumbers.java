/*
 * A number is called Disarium if sum of its digits powered with their respective positions is equal to the number itself.
 * Example: 135 = 1^1 + 3^2 + 5^3.
 * The complete list of Disarium numbers is:
 * 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 89, 135, 175, 518, 598, 1306, 1676, 2427, 2646798, 12157692622039623539 (this last one is too big to be an int)
 * So a solution would be to keep that information in a static final variable, but that wouldn't be fun.
 */
package aprendendojava.sololearn;

import aprendendojava.positiveintegers.PositiveIntegerMethods;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author luiz
 */
public class DisariumNumbers {
    
    public static boolean check(int n) {
        return n == disariumSum(n);
    }
    
    public static List<Integer> list(int upperBound) {
        List<Integer> list = new ArrayList<>();
        for (int n = 0; n < upperBound; n++) {
            if (check(n)) {
                list.add(n);
            }
        }
        return list;
    }
    
    public static int disariumSum(int n) {
        int sum = 0;
        for (int power = PositiveIntegerMethods.nDigits(n); power > 0; power--) {
            sum += PositiveIntegerMethods.pow(n%10, power);
//            System.out.println("sum = " + sum);;
            n /= 10;
//            System.out.println("n = " + n);
        }
        return sum;
    }
    
}
