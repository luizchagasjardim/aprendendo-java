/*

Problem taken from https://codingbat.com/prob/p130788

Given 3 int values, a b c, return their sum.
However, if one of the values is 13 then it does not count towards the sum
and values to its right do not count.
So for example, if b is 13, then both b and c do not count.

*/
package aprendendojava.codingbat.logic;

/**
 *
 * @author luiz
 */
public class LuckySum {

    //I'm generalizing this problem to an int array of any size
    public static int luckySum(int array[]) {
        int sum = 0;
        
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 13) {
                break;
            }
            sum += array[i];
        }
        
        return sum;
    }
    
}
