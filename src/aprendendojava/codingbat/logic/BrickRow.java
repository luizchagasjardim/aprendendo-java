/*

Problem taken from https://codingbat.com/prob/p183562

We want to make a row of bricks that is goal inches long.
We have a number of small bricks (1 inch each) and big bricks (5 inches each).
Return true if it is possible to make the goal by choosing from the given bricks.
This is a little harder than it looks and can be done without any loops.

*/
package aprendendojava.codingbat.logic;

import aprendendojava.positiveintegers.PositiveIntegerMethods;

/**
 *
 * @author luiz
 */
public class BrickRow {
    
    //This generalizes the problem to any brick size.
    //The problem says it can be done without loops, but I don't think that's
    //still true if you change the sizes, so I'm using a loop.
    int smallSize;// = 1;
    int largeSize;// = 5;
    
    BrickRow(int small, int large) {
        smallSize = small;
        largeSize = large;
        if (largeSize < smallSize) {
            System.err.println("Small brick must be smaller than large brick");
        }
    }
    
    public boolean makeBrickRow(int nSmall, int nLarge, int goal) {

        //check if there are enough bricks
        if (nLarge*largeSize + nSmall*smallSize < goal) {
            return false;
        }
        
        //check if any of the inputs is negative
        //no need to check goal
        if (nLarge < 0 || nSmall < 0) {
            return false;
        }

        //check if there is an integer solution to the equation
        //x*largeSize + y*smallSize = goal
        if (goal%PositiveIntegerMethods.gdc(smallSize, largeSize) != 0) {
            return false;
        }
        
        //check if there is a solution to the problem.
        //there's an easy solution which is to just test all possible combinations,
        //but I tried to write a solution which is faster for larger nSmall and
        //nLarge
        Integer[] a = PositiveIntegerMethods.gdcEqSolution(smallSize, largeSize);
        for (int i = 0; i < 3; i++) {
            a[i] *= goal/a[2];
        }
        if (a[0] < 0 && a[1] < 0) return false;
        while (a[0] < 0) {
            a[0] += largeSize;
            a[1] -= smallSize;
            if (a[1] < 0) return false;
        }
        while (a[1] < 0) {
            a[0] -= largeSize;
            a[1] += smallSize;
            if (a[0] < 0) return false;
        }

        return (a[0] <= nSmall && a[1] <= nLarge);

    }
    
}
