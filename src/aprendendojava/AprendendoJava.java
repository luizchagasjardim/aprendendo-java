package aprendendojava;

//import aprendendojava.romannumerals.RomanNumerals;
//import java.util.Comparator;
//import java.util.function.Function;

/**
 *
 * @author luiz
 */
public class AprendendoJava {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //InitializationBLock
//        InitializationBlock.staticMethod();
//        InitializationBlock init = new InitializationBlock();;
//        init.print();
//        init = new InitializationBlock(3);
//        init.print();
//        /*InitializationBlockSub*/ init = new InitializationBlockSub();
//        init.print();

        //Memory Leak
//        StackRuim st = new StackRuim();
//        for (long i = 0; i < 100000000; i++) {
//            st.push(i);
//        }
//        while (!st.empty()) {
//            st.pop();
//        }

//        //Comparator
//        PhoneNumber pn1 = new PhoneNumber(1, 2, 3);
//        PhoneNumber pn2 = new PhoneNumber(1, 2, 3);
//        System.out.println(pn1.compareTo(pn2));

        //Function
//        Function<Double, Integer> f = x -> x.intValue();
//        System.out.println(f.apply(1.7));
//        Function<Integer, Double> g = x -> x.doubleValue();
//        System.out.println(f.andThen(g).apply(1.7));
//        Integer n = Integer.MAX_VALUE;
//        System.out.println(n);
//        for (int i = 0; i < 100000; i++) {
//            n = f.compose(g).apply(n);
//        }
//        System.out.println(n);

    }
    


}
