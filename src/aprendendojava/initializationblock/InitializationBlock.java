/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aprendendojava.initializationblock;

/**
 *
 * @author luiz
 */
public class InitializationBlock {
    
    private int num = 3;
    private String name = "3";
    protected double value = 3;
    static private String className = "nada! (nome dado pela classe)";
    
    static {
        System.out.println("Initialization Block: bloco static rodando");
        className = "Initialization Block! (nome dado pelo bloco static)";
    }
    
    {
        System.out.println("Initialization Block: bloco rodando");
        num = 2;
        name = "2";
        value = 2;
    }
    
    InitializationBlock() {
        System.out.println("Initialization Block: construtor sem argumentos rodando");
        num = 1;
        name = "1";
    }
    
    InitializationBlock(int _num) {
        System.out.println("Initialization Block: construtor com um argumento rodando");
        num = _num;
    }
    
    public static void staticMethod() {
        System.out.println("Initialization Block: método static rodando");
    }
    
    public void print() {
        System.out.println("Initialization Block: método rodando");
        System.out.println("num = " + num);
        System.out.println("name = " + num);
        System.out.println("value = " + num);
        System.out.println("className = " + className);
    }
}