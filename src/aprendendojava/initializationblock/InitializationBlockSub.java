/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aprendendojava.initializationblock;

/**
 *
 * @author luiz
 */
public class InitializationBlockSub extends InitializationBlock {
    
    static {
        System.out.println("Initialization Block Sub: bloco static sub rodando");
    }
    
    {
        System.out.println("Initialization Block Sub: bloco sub rodando");
        value = 4;
    }

    InitializationBlockSub() {
        super(4);
        System.out.println("Initialization Block Sub: contrutor sub rodando");
    }
}
